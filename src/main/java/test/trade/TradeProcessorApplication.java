package test.trade;

import com.google.inject.Guice;
import com.google.inject.Injector;
import test.trade.tradeprocessor.TradeProcessingUnit;
import test.trade.tradeprocessor.configuration.Configuration;
import test.trade.tradeprocessor.module.TradeProcessorModule;

public class TradeProcessorApplication {

    public static void main(String args[]) {
        try {
            Injector injector = Guice.createInjector(new TradeProcessorModule());
            TradeProcessingUnit tradeProcessingUnit = injector.getInstance(TradeProcessingUnit.class);
            tradeProcessingUnit.setConfig(Configuration.ERROR_FILE, "./error.txt");
            tradeProcessingUnit.setConfig(Configuration.AGGREGATION_FILE, "./aggregated.txt");
            tradeProcessingUnit.processTrade("/Users/nhquan/Downloads/TradeInput.csv");
//            System.exit(0);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
